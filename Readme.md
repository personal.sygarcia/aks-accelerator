# AKS-Accelerator

A project to demonstrate:

- Building a 'Hello World' Application
- Manually Containerising a 'Hello World' Application
- Creation of Azure Kubernetes Service (AKS) & Connection to Gitlab CI/CD
- Manually Deploying an Application on AKS
- Automated build & deployment using Gitlab CI/CD
- Troubleshooting

#### Getting Started
- [00 - Prerequisites](docs/00_prerequisites.md)

#### Building a 'Hello World' Application
- [01 I - Writing a “Hello World” Application in Spring Boot](docs/01_I_create_hello_world_java.md)
- [01 II - Writing a “Hello World” Application in .NET](docs/01_II_create_hello_world_dotnet.md)

#### Manually Containerising a 'Hello World' Application
- [02 - Containerising a “Hello World” Application using Docker Desktop](docs/02_containerising.md)
- [03 - Manually Pushing a “Hello World” Image to Gitlab Container Registry](docs/03_pushing_to_gitlab_container_registry.md)

#### Creation of Azure Kubernetes Service (AKS) & Connection to Gitlab CI/CD
- [04 I - Creating an AKS Cluster Using Terraform or Azure CLI](docs/04_I_creating_aks_via_terraform.md)
- [04 II - Creating an AKS Cluster Using the Azure Portal](docs/04_II_creating_aks_via_azure_portal.md)
- [05 - Connecting Gitlab to AKS](docs/05_connecting_gitlab_to_aks.md)

#### Manually Deploying an Application on AKS
- [06 - Manually Deploying a "Hello World" Application to AKS](docs/06_deploying_to_aks.md)

#### Automated build & deployment using Gitlab CI/CD
- [07 - Using Gitlab CI/CD to build an Image](docs/07_gitlab_ci_build_image.md)
- [08 - Using Gitlab CI/CD to deploy an Image](docs/08_gitlab_ci_deploy_image.md)
- [09 - Troubleshooting Advice](docs/10_troubleshooting_deployments.md)

See [Prerequisites](./docs/00_prerequisites.md), or dive right in to [Creating Hello World](docs/01_I_create_hello_world_java.md).

YouTube CI/CD Tutorial
https://www.youtube.com/watch?v=_H7qYkAt8Bo
