# 04 I - Creating an AKS Cluster Using Terraform or Azure CLI
_Aim: to set up Azure Kubernetes Service using either Terraform or the Azure CLI._

 > Note: to follow along with this section, you will need to have Terraform and/or the Azure CLI installed. See [Prerequisites](00_prerequisites.md).

## 4.0 Introduction
In order to create an Azure Kubernetes Service (AKS) cluster, we need to create or reference several Azure objects:

| Resource Type | Purpose | 
| --- | --- | 
| **Cluster Resource Group** | To contain all of the resources we need for our cluster (for simpler management and for operational benefits) |
| **Log Analytics Workspace** | To enable us to retrieve and analyse logs emitted by the cluster |
| **Kubernetes Service** | The managed Kubernetes cluster itself |
| **Network Resource Group** | To contain all of the resources we need for our cluster's network (for simpler management and for operational benefits) |
| **Cluster Vnet** | The Virtual Network into which we will deploy our cluster's resources | 
| **Cluster Subnet** | The Subnet (within the above Virtual Network) into which we will deploy our cluster's resources | 
| **AAD Application** | An application in Azure Active Directory (AAD) for which we will register a Service Principal |
| **Service Principal** | The identity AKS will use to create Azure resources under the hood | 


## 4.1 Using Terraform

### 4.1.1 A Note on Terraform
To manage the complexity and inter-dependencies of these objects, [Hashicorp Terraform](https://www.hashicorp.com/products/terraform/) is a useful tool.

It allows us to declare the infrastructure we want (the "desired state"), provision it, and then iterate upon it or destroy it, as required, in an idempotent manner. 

Achieving the same goals with Azure Resource Manager (ARM) templates, Azure CLI or indeed alternatives such as [Pulumi](http://pulumi.io) is also possible, but Terraform and Azure CLI are described here. 

> Note: the documentation in this repository is not intended to be a complete guide to Terraform, but the Terraform Configurations are commented, where necessary.


### 4.1.2 Terraform Commands
Using Terraform is typically a three stage process:

> Note: no Terraform server is required to use Terraform - only the local `terraform` executable.

| Stage  | Command | Purpose | 
| --- | --- | --- |
| **Init** | `terraform init` | Needs running once, or when new modules or providers are required | 
| **Plan** | `terraform plan -var-file=secrets.tfvars` | Use to 'dry run' changes to infrastructure |
| **Apply** | `terraform apply -var-file=secrets.tfvars` | Use to implement changes to infrastructure |


### 4.1.3 Terraform Secrets
It is bad practice to put usernames and passwords ("secrets") in a git source repository. Here, we take the simple approach of using a file called `secrets.tfvars` (the name is unimportant), stored in the root of the project, but not committed, to declare our private preferences. We exclude it from being committed into git by using a `.gitignore` file.

```
service_principal_password = "<a service principal password>"
```

### 4.1.4 Authenticating with Azure

Terraform can authenticate with Azure using either a Service Principal or using Azure CLI. When running locally, [Azure CLI is recommended](https://www.terraform.io/docs/providers/azurerm/auth/azure_cli.html). Thus, before running `terraform init`, use `az login`:

```
➜  az login

Note, we have launched a browser for you to login. For old experience with device code, use "az login --use-device-code"
You have logged in. Now let us find all the subscriptions to which you have access...
```

If you have more than one Subscription, make sure to choose the one you would like to use:

```
➜  az account set --subscription <name or ID of Subscription>
```

### 4.1.5 Creating our Infrastructure

Running `terraform apply -var-file=secrets.tfvars --auto-approve` in this repository creates the infrastructure we need. Full creation took 9 minutes and 10 seconds, but your execution time will vary depending on the responsiveness of Azure at the time you run it.

```
➜  terraform apply --var-file=secrets.tfvars
random_integer.entropy: Refreshing state... (ID: 465)
random_integer.entropy: Refreshing state... (ID: 243)
azuread_application.aks_app: Refreshing state... (ID: 1d7c7989-b6d3-48b6-912c-127209841e50)
azuread_service_principal.aks_app_service_principal: Refreshing state... (ID: dde90f75-1292-4301-a3ee-f5edcb177e3f)
azuread_service_principal_password.aks_app_sp_password: Refreshing state... (ID: dde90f75-1292-4301-a3ee-f5edcb177e3f/0b9a2878-d879-7818-75a9-bab687c15cb7)
azurerm_resource_group.aks_rg: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...6fae/resourceGroups/aks-accelerator-rg)
azurerm_resource_group.aks_vnet_rg: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...resourceGroups/aks-accelerator-vnet-rg)
data.azurerm_subscription.primary: Refreshing state...
azurerm_role_assignment.aks_network_contributor: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...s/5dd9ea44-4453-da08-f0b4-75a1e9776dab)
azurerm_virtual_network.aks_vnet: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...k/virtualNetworks/aks-accelerator-vnet)
azurerm_log_analytics_workspace.aks_law: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...hts/workspaces/aks-accelerator-243-law)
azurerm_storage_account.aks_storage_account: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...ge/storageAccounts/aksaccelerator465sa)
azurerm_subnet.aks_subnet: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...-vnet/subnets/aks-accelerator-vnet-sn1)
azurerm_kubernetes_cluster.aks_cluster: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...ce/managedClusters/aks-accelerator-aks)
azurerm_monitor_diagnostic_setting.aks_diagnostics: Refreshing state... (ID: /subscriptions/1f2c30f4-905c-45ff-9417-...erator-aks|aks-accelerator-diagnostics)

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  ~ update in-place

Terraform will perform the following actions:

...
```



## 4.2 Using Azure CLI

To create a cluster using Azure CLI, we must create the components (listed at the top of this page) separately.

To create AKS itself, we can use the following:

```
az aks create \
--name aks-accelerator-aks  \
--resource-group aks-accelerator-rg \
--enable-addons http_application_routing, monitoring \
--workspace-resource-id <object id> \
--kubernetes-version 1.12.6 \
--location westeurope \
--dns-name-prefix aks-accelerator-aks \
--node-vm-size Standard_D2_v3 \
--nodepool-name aks-kscclrtr \
--max-pods 20 \
--network-plugin azure \
--pod-cidr 192.168.0.1/24 \
--service-principal <object id> \
--vnet-subnet-id <object id>
```

> Note: notice that, with the CLI, RBAC is enabled by default, whereas in the Azure Portal it is not.

Terraform is beneficial for complex use cases such as this because we can not only declare our cluster configuration, but also all of the cluster's dependencies. Terraform will manage the order in which those dependencies must be created.

Doing this efficiently with a hand-crafted script would be arguably more complex, and definitely less flexible & repeatable; it is not recommended for cases such as this.


## 4.3 Testing Access to the Cluster


### 4.3.1 Installing Kubectl
> If you have `kubectl` installed already, skip this step.

If you do not already have `Kubectl`, the Kubernetes CLI, the Azure CLI provides a simple way to install it:

```
➜  az aks install-cli
```

### 4.3.2 Configuring Kubectl

The Azure CLI also provides a simple mechanism for configuring `~/.kube/config` (the `kubectl` config file) with credentials for the newly created cluster:

```
➜  az aks get-credentials  \
--name aks-accelerator-aks  \
--resource-group aks-accelerator-rg
```

This command will retrieve, using Azure CLI credentials, the necessary secrets from Kubernetes and merge them into the local config map (alongside the details for any existing clusters):

```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: <base64 encoded cert>
    server: https://aks-accelerator-<id>.hcp.westeurope.azmk8s.io:443
  name: aks-accelerator-aks
contexts:
- context:
    cluster: aks-accelerator-aks
    user: clusterUser_aks-accelerator-rg_aks-accelerator-aks
  name: aks-accelerator-aks
current-context: aks-accelerator-aks
kind: Config
preferences: {}
users:
- name: clusterUser_aks-accelerator-rg_aks-accelerator-aks
  user:
    client-certificate-data: <base64 encoded cert>
    client-key-data: <base64 encoded key>
    token: <token>
```  

At this point, kubectl's context is set correctly. If you need to check it:

```
➜  kubectl config current-context

aks-accelerator-aks
```
 

### 4.3.3 Connecting to the Cluster

We can now connect to the cluster:

```
➜  kubectl cluster-info

Kubernetes master is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443
addon-http-application-routing-default-http-backend is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/addon-http-application-routing-default-http-backend/proxy
addon-http-application-routing-nginx-ingress is running at http://xx.xx.xx.xx:80 http://xx.xx.xx.xx:443
Heapster is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/heapster/proxy
CoreDNS is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
kubernetes-dashboard is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/kubernetes-dashboard/proxy
Metrics-server is running at https://aks-accelerator-xxxxxxxx.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy

```

```
➜  kubectl get nodes

NAME                      STATUS   ROLES   AGE   VERSION
aks-kscclrtr-40561189-0   Ready    agent   43m   v1.12.6
```

> To see what else can be retrieved with `get`, see `kubectl api-resources`.

## 4.4 Potential Issues

Kubectl authenticates with the Kubernetes API server with TLS Mutual (Client Certificate) authentication.

In Enterprise environments, corporate internet proxies sometimes perform SSL interception (SSLi).

If you are not able to authenticate with the API server, check whether SSL interception is in place. If it is, then you may need to ask your network security team for an exception.


## 4.5 Scaling the Cluster

### 4.5.1 Using Terraform
To increase or decrease the number of nodes in the cluster, we can simply change the `agent pool` count:

```
resource "azurerm_kubernetes_cluster" "aks_cluster" {

  "agent_pool_profile" {
    ...
    count   = "3"
    ...
    }
```

### 4.5.2 Using Azure CLI
Using Azure CLI, we can use `az aks scale`:

```
➜  az aks scale \
--name aks-accelerator-aks  \
--resource-group aks-accelerator-rg \
--node-count 3
```



## Next
In the next section, we will configure Gitlab to deploy to AKS.

< [03 Containerising a “Hello World” Application](03_pushing_to_gitlab_container_registry.md) | 04 Creating an AKS Cluster | [05 Connecting Gitlab to AKS](05_connecting_gitlab_to_aks.md) >