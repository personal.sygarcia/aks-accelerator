# 08 - Using Gitlab CI/CD to deploy an Image

## 8.0 Introduction
In this section we will use Gitlab CI/CD to deploy our container image from Gitlab Container Registry to AKS.

## 8.1 Deployment with Gitlab

Relatively speaking, asking Gitlab to deploy our application to AKS is simpler than building the image.

As before, we create a new job, `deploy`, part of our `deploy` stage:

```
deploy:
  stage: deploy
  environment:
    name: staging
```

We need an image that contains `kubectl`. We could roll our own, and may wish to do so in environments where security is paramount. For the purposes of this example, we will leverage Dockerhub:

```
  image:
    name: lachlanevenson/k8s-kubectl
    entrypoint: [""]
```

All that remains is to instruct `kubectl` to create our deployments:

```
script:
- mkdir -p ${KUBEHOME}
- echo ${kube_config} | base64 -d > ${KUBEHOME}/config
- kubectl apply -f ${CI_PROJECT_DIR}/kubernetes/deployment.yaml
- kubectl apply -f ${CI_PROJECT_DIR}/kubernetes/service-public.yaml
- kubectl set image -n gitlab-managed-apps deployment/aks-accelerator-deployment accelerator=registry.gitlab.com/citihub/aks-accelerator:$CI_COMMIT_SHORT_SHA
```

- First, we create a directory for our kube `config` file
- Then, we take the Gitlab-injected `${kube_config}` variable, and create a config file from it
- We then apply our deployment and our services
- Finally, we ask that our containers are updated to our latest build, `registry.gitlab.com/citihub/aks-accelerator:$CI_COMMIT_SHORT_SHA`

## 7.3 Additional Gitlab namespace

Since we didn't specify a particular namespace when we gave Gitlab access to our cluster, Gitlab has created:

- gitlab-managed-apps
- aks-accelerator-xxxxxxxx

```
➜  kubectl get namespaces

NAME                       STATUS   AGE
aks-accelerator-11583841   Active   28h
default                    Active   28h
gitlab-managed-apps        Active   27h
kube-public                Active   28h
kube-system                Active   28h
```

Our most recent deployment is in the `gitlab-managed-apps` namespace, but there are pods in the additional namespace too:

```
➜  kubectl get pods -n aks-accelerator-11583841
NAME                                          READY   STATUS    RESTARTS   AGE
aks-accelerator-deployment-65cf4464dd-sqdpp   1/1     Running   0          16h
aks-accelerator-deployment-65cf4464dd-whvd9   1/1     Running   0          16h
```

Why this is the case requires further investigation. Gitlab did need to have access to this namespace, too, though, so it was granted the `cluster-admin` role.

If this causes a problem, see `kubernetes/template-aks-accelerator-cluster-role-binding.yaml` and edit for your specific namespace and service account name:

```
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: aks-accelerator-xxxxxxxx-admin
subjects:
- kind: ServiceAccount
  name: aks-accelerator-xxxxxxxx-service-account
  namespace: aks-accelerator-xxxxxxxx
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
```



## 7.4 Viewing our Pipeline

We can see the execution of our Job:

![Gitlab Deployment](img/08_Gitlab_Deploy.png "Gitlab Deployment")


## 7.5 A note on Gitlab Runner Compatibility when using Gitlab.com

There is an issue with this pipeline.

- On gitlab-runner 11.8.0, the pipeline works
- On gitlab-runner 11.9.0rc2 (one of the shared runners provided by gitlab.com), it fails:

```
Running with gitlab-runner 11.9.0-rc2 (227934c0)
  on docker-auto-scale 0277ea0f
...

$ docker build -t registry.gitlab.com/citihub/aks-accelerator:latest -f Dockerfile-ci .
time="2019-03-31T21:28:34Z" level=error msg="failed to dial gRPC: cannot connect to the Docker daemon. Is 'docker daemon' running on this host?: dial tcp 0.0.0.0:2375: connect: connection refused"
context canceled
```

The workaround is to retry with 11.8.0.


## Next
In the next section we will demonstrate Continuous Delivery with our new pipeline.

< [06 Manually Deploying a "Hello World" Application to AKS](07_gitlab_ci_build_image.md) | 08 Deploying a Container Image with Gitlab CI/CD | [09 Demonstrating Continuous Delivery](09_continuous_delivery.md) >