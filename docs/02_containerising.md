# 02 - Containerising a “Hello World” Application using Docker Desktop
_Aim: To create a “Hello World” application on a local machine and create an OCI container image using docker._ 


## 2.0 Introduction
Docker offers a set of tools to build, run, manage and share container images.

While the image format is known colloquially as a "Docker image", the real name for the image format is the _Open Container Initiative_ (OCI) specification. There are other vendors and open source projects which are OCI-compliant. Any OCI image is, technically, compatible with any OCI-compliant tooling.

 > Note: to follow along with this section, you must have Docker installed. See [Prerequisites](00_prerequisites.md).



## 2.1 Dockerfiles
To run our application within a container, we must write a `Dockerfile`, the reference documentation for which is [here](https://docs.docker.com/engine/reference/builder/).

Dockerfiles should live with their source code. They typically express the following, but many more options are available.

- The **base image** on top of which we will layer our application code (browse [Dockerhub](http://hub.docker.com) to see examples of base images we could choose, authored either by vendors or by community members)
- The **build commands** to install dependencies inside the image
- The **artifacts** to be copied into the image
- The **entrypoint** for the application (i.e. the command that will be executed by default when running the image)
- The **ports** that the application exposes
- The (storage) **volumes** required by the application

## 2.2 Our Dockerfile

### 2.2.1 Dockerfile

```
FROM openjdk:8-jre-alpine
LABEL maintainer="abhishek.pradhan@citihub.com"
EXPOSE 8080/tcp
ADD build/libs/aks-accelerator.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

### 2.2.2 What does this do?

- We will choose to build our application **FROM** the [Alpine Linux](https://www.alpinelinux.org) base with the [OpenJDK](https://openjdk.java.net) preinstalled.

> Container images can be large (impacting both storage and network transfer times). The Alpine distribution is a popular base for containers because it has small size as a design goal.

- The _maintainer_ **LABEL** isn't mandatory, but serves as an example of the kind of additional metadata that one might wish to add to a Dockerfile

- Our Spring Boot (Tomcat) application runs on port 8080 by default - we use **EXPOSE** to advertise this to users of our Dockerfile

- To ensure that our code artefacts and dependencies are inside our container, we must **ADD** them. Here, we demonstrate renaming our application's .jar to _app.jar_ at the same time.

- To tell our runtime how to launch our application, we then declare the **ENTRYPOINT**. As a Spring Boot application (with embedded application server) we can simply run it with `java -jar`

> Note: the syntax for _ENTRYPOINT_ may initially seem a little strange - you'll notice that it requires an JSON array (designated by `[]`) containing the command followed by the command's arguments


## 2.3 Building a Container Image

With our file in place, we can build our image.

Initially, we will do this by hand, using the Docker command line tool. Later, we will build from our Continuous Integration server.


> Note: if you have not done so yet, start the Docker Desktop application (which will run the Docker daemon)

> Note: whilst the Docker command line tool is available in [Azure Cloud Shell](https://shell.azure.com), the Docker engine is not, so you will not be able to build images in Cloud Shell.

We ask Docker to build the _directory_ that contains our Dockerfile (if you specify the Dockerfile directly, `docker` will complain).

At the same time, we pass the `-t` argument to _tag_ the image we build. This allows us to more easily manage and use our images later, as we will see.

```
➜  docker build . -t aks-accelerator:latest

Sending build context to Docker daemon  122.4MB
Step 1/5 : FROM openjdk:8-jre-alpine
 ---> 3402df656dfa
Step 2/5 : LABEL maintainer="abhishek.pradhan@citihub.com"
 ---> Using cache
 ---> 6e3eb7fc952e
Step 3/5 : EXPOSE 8080/tcp
 ---> Using cache
 ---> 79c17a27bc1c
Step 4/5 : ADD build/libs/aks-accelerator.jar app.jar
 ---> Using cache
 ---> 2cc9dba56842
Step 5/5 : ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
 ---> Using cache
 ---> 2872e8e0caa3
Successfully built 2872e8e0caa3
Successfully tagged aks-accelerator:latest
```

## 2.4 Managing Container Images

To see the output of our build, and any other images that we might have previously built, we can use other `docker` commands.

```
➜  docker images

REPOSITORY                               TAG                 IMAGE ID            CREATED             SIZE
aks-accelerator                          latest              cf4d19082b09        34 minutes ago      102MB
openjdk                                  8-jre-alpine        3402df656dfa        2 days ago          85MB
python                                   2                   c1f0859fc0e0        2 months ago        912MB
openjdk                                  latest              2cbfaac94298        2 months ago        821MBB
python                                   3                   db1c801f1c06        3 months ago        926MB
jvns/pandas-cookbook                     latest              65fafbeadee3        3 months ago        2.91GB
centos                                   7                   1e1148e4cc2c        3 months ago        202MB
gitlab/gitlab-ce                         latest              e420c3fac3e3        11 months ago       1.39GB
```

> Note: notice that our Alpine-based `aks-accelerator` image is relatively small at 102MB, but other images are much bigger - e.g. the Python base is 926MB.

> Note: notice also that our image has been given a `repository` and `tag` in this table, but that they were not declared in our `Dockerfile`.

If we hadn't provided the `-t` argument to `docker build` then we would see something like this, which wouldn't tell us what our image really was.

```
➜  docker build .

➜  docker images
REPOSITORY                               TAG                 IMAGE ID            CREATED             SIZE
<none>                                   <none>              a63f7b1715cd        5 seconds ago       102MB
```

To delete an image we no longer need, we can delete it with familiar commands such as `rm`. Notice how, in doing so, we remove the image in question, and also any intermediate images, or layers, that were generated during our build (Docker keeps these layers around to speed up future builds - if only a single layer, such as our code, is modified, only that layer needs to re-built).

```
➜  docker image rm a63f7b1715cd
Deleted: sha256:a63f7b1715cd7142ddf618cb977ae84293115c3857ee8c69c1a7e4ec0d4a3f91
Deleted: sha256:710cbf6bb4482b921440b7d70c14a84b500b034cc69f7f0ef83e319f851ed44a
Deleted: sha256:586b4502dfb1b8450d825f8bff781ab26b02bf6f0060faa3b4a7c50893eb4129
Deleted: sha256:89505e9f59756fc0a7c2633bfb3858cba6cbb34f608cca466cdcd9904cb35ba1
```

## 2.5 Running a Container Image
Thus far, we have used `docker` to create a Container **Image** from our `Dockerfile`, but we are yet to have a running container (we can think of the Container Images here as analogous to the "image" we might have of a Virtual Machine).

To run our image, we can use `docker run` as follows. We specify the image we wish to run, and also ask Docker to _publish_ (with `-P`) any ports that we exposed via our Dockerfile.

- If we use `-P`, Docker will publish all exposed container ports on random ports on the host
- If we wish to choose specific ports, we can use the lowercase option, e.g. `-p 8080:8080` 

Here, we just have a single port, and have no particular preference as to how it is exposed, so we use `-P`:

```
➜  ~ docker run -P aks-accelerator:latest
```

We will see the familiar Spring startup log, followed by a message telling us that our application has started:


```
➜  ~ docker run -P aks-accelerator:latest

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.3.RELEASE)

Starting Application on febab4c462f7 with PID 1 (/app.jar started by root in /)

...

Tomcat started on port(s): 8080 (http) with context path ''
Started Application in 4.792 seconds (JVM running for 5.811)

```

## 2.6 Using our Application
With our application running, we can now test it with our browser or with the command line, but first we must find the port on which it was exposed:

```
➜  docker ps

CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS              PORTS                     NAMES
febab4c462f7        aks-accelerator:latest   "java -Djava.securit…"   7 minutes ago       Up 7 minutes        0.0.0.0:32769->8080/tcp   gallant_stallman
```

We see that Docker has randomly assigned port `32769` to our running container, and that it maps to port `8080` in our application.

We can test our containerised application as follows:

```
➜  ~ curl -X GET http://127.0.0.1:32769
Just hello world. Application demonstrates Gitlab, K8s integration.
```

## 2.7 Running Containers in the Background
Having launched our containerised application for the first time, and having proved that it is working, we may wish to launch it in the background.

For a reminder of how to do this, we can check the built-in help, and notice that the option we need is `-d`.

```
➜  docker run --help | grep background
  -d, --detach                         Run container in background and print container ID
```

Let's re-launch our application. Notice that, this time, Docker has assigned a different random port - 32770:

```
➜  ~ docker run -dP aks-accelerator:latest

fe50a29b337aacda5e36b7047b43137f36c381a4369008f37bf08935298c7589

➜  ~ curl -X GET http://127.0.0.1:32769
curl: (7) Failed to connect to 127.0.0.1 port 32769: Connection refused

➜  ~ docker ps

CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS              PORTS                     NAMES
fe50a29b337a        aks-accelerator:latest   "java -Djava.securit…"   10 seconds ago      Up 9 seconds        0.0.0.0:32770->8080/tcp   affectionate_cray

➜  ~ curl -X GET http://127.0.0.1:32770
Just hello world. Application demonstrates Gitlab, K8s integration.
```

## Next
In the next section, we will push our image to Gitlab's container registry.

< [01 Writing a “Hello World” Application](01_I_create_hello_world_java.md) | 02 Containerising a “Hello World” Application | [03 Pushing a “Hello World” Application Container Image to Gitlab Container Registry](03_pushing_to_gitlab_container_registry.md) >