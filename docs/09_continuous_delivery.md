# 09 Demonstrating Continuous Delivery 

## 9.0 Introduction
Now that our pipeline is in place, we can demonstrate it end-to-end:

- Make a code change (to respond with a new 'hello world' message)
- Watch the pipeline complete
- Watch our code automatically update with the new message

Since this will happen relatively quickly, we will set it up in reverse:

## 9.1 Watching our Existing Service

Locate our existing deployment with `kubectl`:

```
➜  kubectl get services -n gitlab-managed-apps
NAME                      TYPE           CLUSTER-IP    EXTERNAL-IP    PORT(S)        AGE
aks-accelerator-service   LoadBalancer   10.0.10.62    40.118.81.46   80:31037/TCP   26h
tiller-deploy             ClusterIP      10.0.71.162   <none>         44134/TCP      26h
```

Using `curl`, we can continuously poll this endpoint:

```
➜  while true; do curl -X GET http://40.118.81.46; sleep 2; echo "\n"; done

Just hello world. Application demonstrates Gitlab, K8s integration.

Just hello world. Application demonstrates Gitlab, K8s integration.

Just hello world. Application demonstrates Gitlab, K8s integration.

...
```

For now, leave this running.

## 9.2 Making a Code Change

We can now edit our controller to respond with a new message:

```
@GetMapping(path="/")
public String getHome() {
    return "Hello World. I've just been deployed by Gitlab.";
}
```

If we commit and push this:

```
➜  git add .

➜  git commit -m "Updated Controller message to exercise continuous deployment"

➜  git push origin master
```

## 9.3 Watching our Pipeline

And then watch our pipeline:

![Gitlab Pipeline in Progress (Build)](img/09_Pipeline_Build.png "Gitlab Pipeline in Progress (Build)")
![Gitlab Pipeline in Progress (Push)](img/09_Pipeline_Push.png "Gitlab Pipeline in Progress (Push)")
![Gitlab Pipeline in Progress (Deploy)](img/09_Pipeline_Deploy.png "Gitlab Pipeline in Progress (Deploy)")


## 9.4 Rolling Update

We can watch Kubernetes perform a rolling update of our Pods (scheduling new ones, removing old ones):

```
➜  kubectl get pods -n gitlab-managed-apps --watch --selector app=accelerator --watch
```

Our Pods ("8qjfx" and "nsv6n") are initially running:
```
NAME                                          READY   STATUS    RESTARTS   AGE
aks-accelerator-deployment-7964dff6dd-8qjfx   1/1     Running   0          3m44s
aks-accelerator-deployment-7964dff6dd-nsv6n   1/1     Running   0          3m37s
```

A new Pod ("q7zdd") is scheduled:
```
aks-accelerator-deployment-684f4b7fc7-q7zdd   0/1   Pending   0     1s
aks-accelerator-deployment-684f4b7fc7-q7zdd   0/1   Pending   0     1s
```

And then created:
```
aks-accelerator-deployment-684f4b7fc7-q7zdd   0/1   ContainerCreating   0     1s
aks-accelerator-deployment-684f4b7fc7-q7zdd   1/1   Running   0     8s
```

Once running, the Pod it is replacing ("nsv6n") is terminated:

```
aks-accelerator-deployment-7964dff6dd-nsv6n   1/1   Terminating   0     7m36s
```

And then the process repeats for the other Pod in our ReplicaSet. "tw7xz" is created to replace "8qjfx"

```
aks-accelerator-deployment-684f4b7fc7-tw7xz   0/1   Pending   0     0s
aks-accelerator-deployment-684f4b7fc7-tw7xz   0/1   Pending   0     0s
aks-accelerator-deployment-684f4b7fc7-tw7xz   0/1   ContainerCreating   0     0s
aks-accelerator-deployment-7964dff6dd-8qjfx   1/1   Terminating   0     7m49s
aks-accelerator-deployment-7964dff6dd-8qjfx   0/1   Terminating   0     7m52s
```

## 9.5 Watching our Application

As the change rolls in, we initially see the old message gradually replaced by the new message.

We see that the change is not _quite_ seamless - there is a small period of downtime (failed connections). To help with this, we could look at using Kubernete Readiness Probes.

```
Just hello world. Application demonstrates Gitlab, K8s integration.

Just hello world. Application demonstrates Gitlab, K8s integration.

curl: (7) Failed to connect to 40.118.81.46 port 80: Connection refused


Just hello world. Application demonstrates Gitlab, K8s integration.

Just hello world. Application demonstrates Gitlab, K8s integration.

curl: (7) Failed to connect to 40.118.81.46 port 80: Connection refused


curl: (7) Failed to connect to 40.118.81.46 port 80: Connection refused


Hello World. I've just been deployed by Gitlab.

Hello World. I've just been deployed by Gitlab.
```


## Next
In the next section we discuss various tips for troubleshooting problems that can occur when replicating this process.

< [08 Deploying a Container Image with Gitlab CI/CD](08_gitlab_ci_deploy_image.md) | 09 Demonstrating Continuous Delivery | [10 Troubleshooting Deployments](10_troubleshooting_deployments.md) >