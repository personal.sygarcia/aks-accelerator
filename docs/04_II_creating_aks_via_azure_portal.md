# 04 II - Creating an AKS Cluster Using the Azure Portal
_Aim: to set up Azure Kubernetes Service using the Azure Portal._

## 4.0 Introduction
It can be useful to use the Azure Portal when creating a resource for the first time, or to inspect the options for an unfamiliar resource.

Here, we show the process of creating an equivalent Azure Kubernetes Service instance using the portal.

## 4.1 Creating a Cluster

### 4.2.1 Basics
![The Basics Tab](img/04_II_A_Basics.png "The Basics Tab")

| Field | Comments |
| --- | --- | 
| **Kubernetes Cluster Name** | Must be unique within the resource group |
| **Region** | When choosing an Azure Region, consider the latency to your users, but also whether or not Preview features are available |
| **Kubernetes Version** | 1.12.6 is, at the time of writing, the most up-to-date supported by AKS although Kubernetes 1.14 is now available |
| **DNS Name Prefix** | The prefix for the name that will be used for the API server endpoint, i.e. https://DNS_NAME_PREFIX-UNIQUE_ID.hcp.REGION.azmk8s.io:|
| **Node Size** | The VMs to use for your cluster |
| **Node Count** | The number of VMs to use for your cluster | 
| **Virtual Nodes** | A [Preview feature](https://go.microsoft.com/fwlink/?linkid=2016182) - whether or not to burst out to Azure Container Instance |

### 4.2.2 Basics
![The Authentication Tab](img/04_II_B_Authentication.png "The Authentication Tab")

| Field | Comments |
| --- | --- | 
|Service Principal | If required, AKS will create one for you. If you'd prefer to provide an existing one (ID & Secret), that's possible too. |
|Enable RBAC | Whether to enable Kubernetes Role Based Access Control |

### 4.2.3 Network
![The Network Tab](img/04_II_C_Networking.png "The Network Tab")

| Field | Comments |
| --- | --- | 
| HTTP Application Routing | A [feature that provides simple Ingress Controllers](https://go.microsoft.com/fwlink/?linkid=2032149), suitable in non-prod environments |
| Network Configuration | "Basic" offers a fully managed network, "Advanced" allows deployment into a network of the user's choosing |
| Virtual Network | For Advanced Networking, the name of the Virtual Network into which AKS should be deployed |
| Cluster Subnet  | For Advanced Networking, the name of the Subnet (within the above VNET) into which AKS should be deployed |
| Kubernetes Service Address Range | For Advanced Networking, CIDR notation for Services. Must not overlap with the Subnet above |
| Kubernetes DNS Service Address | For Advanced Networking, must be within the Service address range. The IP address for the Kubernetes DNS. |
| Docker Bridge address | For Advanced Networking. Must not overlap with the Subnet or Service Address range above. |

### 4.2.4 Monitoring
![The Monitoring Tab](img/04_II_D_Monitoring.png "The Monitoring Tab")

| Field | Comments |
| --- | --- | 
| Enable Container Monitoring | Whether or not to enable additional Log Analytics |
| Log Analytics Workspace | The name of the workspace into which to send diagnostics |  
