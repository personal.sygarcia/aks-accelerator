package com.citihub.cloud;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping(path="/")
    public String getHome() {
        return "Continuous Deployment is great.";
    }

}
