data "azurerm_subscription" "primary" {}

/*
Type: Azure AD Application
Purpose: Registration of an Application is required in order to create a Service Principal
*/
resource "azuread_application" "aks_app" {
  name = "${format("%s-app", var.project_name)}"
}

/*
Type: Service Principal
Purpose: AKS must manage additional Azure resources (e.g. Load Balancers) and thus requires an identity with suitable roles
*/
resource "azuread_service_principal" "aks_app_service_principal" {
  application_id = "${azuread_application.aks_app.application_id}"
}

/*
Type: Service Principal Password
Purpose: The password must also be made available to AKS
*/
resource "azuread_service_principal_password" "aks_app_sp_password" {
  service_principal_id = "${azuread_service_principal.aks_app_service_principal.id}"
  value                = "${var.service_principal_password}"
  end_date             = "2020-01-01T01:02:03Z"

  // See https://github.com/terraform-providers/terraform-provider-azuread/issues/4
  // azurerm_role_assignment fails to work (commented out below)
//  provisioner "local-exec" {
//    command = "az role assignment create --role 'Network Contributor' --assignee-object-id ${azuread_service_principal.aks_app_service_principal.id} --scope ${data.azurerm_subscription.primary.id}"
//  }
}


resource "azurerm_role_assignment" "aks_network_contributor" {
  role_definition_name = "Network Contributor"
  principal_id         = "${azuread_service_principal.aks_app_service_principal.id}"
  scope                = "${data.azurerm_subscription.primary.id}"
}