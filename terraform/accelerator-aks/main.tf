locals {
  // The Kubernetes Agent Pool name must start with a lowercase letter, have max length of 12, and only have characters a-z0-9
  // - We remove the illegal characters
  agent_pool_name_legal_chars = "${replace(lower(var.project_name),"/[^a-z0-9]/","")}"
  // - Then remove the vowels, to shorten to 12
  // - Alternative approaches are available!
  agent_pool_name_shortened = "${replace(local.agent_pool_name_legal_chars, "/[aeiou]/", "")}"
}

/*
Type: Resource Group
Purpose: To provide a container for AKS and its dependencies
*/
resource "azurerm_resource_group" "aks_rg" {
  location = "${var.location}"
  name     = "${format("%s-rg", var.project_name)}"
}

/*
Type: Log Analytics Workspace
Purpose: To allow analysis of logs & metrics emitted by AKS
*/

// Log Analytics Workspaces have a public endpoint, and therefore must have a unique FQDN
// Generate a random integer to append to the name
resource "random_integer" "entropy" {
  min     = 1
  max     = 999
}


resource "azurerm_log_analytics_workspace" "aks_law" {
  location            = "${var.location}"
  name                = "${format("%s-%s-law", var.project_name, random_integer.entropy.result)}"
  resource_group_name = "${azurerm_resource_group.aks_rg.name}"

  # SKU can be Free, PerNode, Premium, Standalone, Standard, Unlimited, PerGB2018
  # See http://aka.ms/PricingTierWarning if you receive a message that your chosen SKU is incompatible with your Subscription
  sku = "PerGB2018"
}

/*
Type: Azure Kubernetes Cluster
Purpose: The cluster itself, in Advanced Networking Mode (for private networking), with RBAC enabled
*/
resource "azurerm_kubernetes_cluster" "aks_cluster" {

  "agent_pool_profile" {
    name    = "${local.agent_pool_name_shortened}"
    os_type = "Linux"
    vm_size = "Standard_D2_v3"
    count   = "3"

    // Note:
    // The address space required for the cluster depends on the number of nodes in the cluster and the number of
    // pods per node. Also be aware that AKS runs its own pods by default (for operation, monitoring, logging).
    // A '/27' network is too small for Kubernetes.
    max_pods = 20
    vnet_subnet_id = "${var.aks_vnet_subnet_id}"
  }

  // Note:
  // Important Portal vs CLI difference:
  // - CLI - when specifying a Vnet (for 'Advanced Networking' mode) it is possible to miss that 'network_plugin' is also required
  // - Portal - the network plugin is set correctly when enabling 'Advanced Networking'
  network_profile {
    network_plugin     = "azure"
    # pod_cidr           = "192.168.0.1/24"
    # `docker_bridge_cidr`, `dns_service_ip` and `service_cidr` should all be empty or all should be set.
  }

  dns_prefix          = "${var.project_name}"
  location            = "${var.location}"
  name                = "${format("%s-aks", var.project_name)}"
  resource_group_name = "${azurerm_resource_group.aks_rg.name}"

  // Note:
  // At the time of writing (March 2019), K8s 1.14 is newly released, but not yet supported by AKS
  // 1.14 supports Windows Server containers, Pod priorities + preemption
  kubernetes_version = "1.12.6"

  // Note:
  // AKS requires at minimum a Service Principal that has been granted the Network Contributor role
  // Additional roles may also be needed:
  // - e.g. to pull from an Azure Container Registry, the ACR Pull role is needed
  "service_principal" {
    client_id     = "${var.service_principal}"
    client_secret = "${var.service_principal_password}"
  }

  addon_profile {
    // Note:
    // For a test cluster, http_application_routing is a useful
    // It will provide a default ingress controller and save us from needing further configuration
    // Microsoft do not recommend it in Production (consider Nginx, Istio, etc.)
    http_application_routing {
      enabled = true
    }

    // Note:
    // Objects provisioned by AKS (e.g. Azure Load Balancers) can fail, typically due to misconfiguration
    // Log Analytics can give us access to the K8s Cloud Controller Manager logs which are useful for troubleshooting
    oms_agent {
      enabled                    = true
      log_analytics_workspace_id = "${azurerm_log_analytics_workspace.aks_law.id}"
    }
  }

  // Note:
  // Important Portal vs CLI difference:
  // - CLI - RBAC is enabled by default (must pass --disable-rbac to disable)
  // - Portal - RBAC is disabled by default
  role_based_access_control {
    enabled = true
  }
}
